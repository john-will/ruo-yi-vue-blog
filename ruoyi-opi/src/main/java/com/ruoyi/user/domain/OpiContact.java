package com.ruoyi.user.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 联系我们对象 opi_contact
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
public class OpiContact
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 公司 */
    @Excel(name = "公司")
    private String company;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 经度 */
    @Excel(name = "经度")
    private Long lng;

    /** 纬度 */
    @Excel(name = "纬度")
    private Long lat;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String email;

    /** 电话 */
    @Excel(name = "电话")
    private String tel;

    /** 传真 */
    @Excel(name = "传真")
    private String fax;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCompany(String company) 
    {
        this.company = company;
    }

    public String getCompany() 
    {
        return company;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setLng(Long lng) 
    {
        this.lng = lng;
    }

    public Long getLng() 
    {
        return lng;
    }
    public void setLat(Long lat) 
    {
        this.lat = lat;
    }

    public Long getLat() 
    {
        return lat;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setFax(String fax) 
    {
        this.fax = fax;
    }

    public String getFax() 
    {
        return fax;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("company", getCompany())
            .append("address", getAddress())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("email", getEmail())
            .append("tel", getTel())
            .append("fax", getFax())
            .toString();
    }
}
