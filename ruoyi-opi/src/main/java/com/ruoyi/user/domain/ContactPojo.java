package com.ruoyi.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactPojo {

    // 主键
    private Long id;

    // 公司
    private String company;

    // 地址
    private String address;

    // 经度
    private String lng;

    // 纬度
    private String lat;

    // 邮箱
    private String email;

    // 电话
    private String tel;

    // 传真
    private String fax;
}
