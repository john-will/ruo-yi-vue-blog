package com.ruoyi.user.service;

import java.util.List;
import com.ruoyi.user.domain.OpiContact;

/**
 * 联系我们Service接口
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
public interface IOpiContactService 
{
    /**
     * 查询联系我们
     * 
     * @param id 联系我们主键
     * @return 联系我们
     */
    public OpiContact selectOpiContactById(Long id);

    /**
     * 查询联系我们列表
     * 
     * @param opiContact 联系我们
     * @return 联系我们集合
     */
    public List<OpiContact> selectOpiContactList(OpiContact opiContact);

    /**
     * 新增联系我们
     * 
     * @param opiContact 联系我们
     * @return 结果
     */
    public int insertOpiContact(OpiContact opiContact);

    /**
     * 修改联系我们
     * 
     * @param opiContact 联系我们
     * @return 结果
     */
    public int updateOpiContact(OpiContact opiContact);

    /**
     * 批量删除联系我们
     * 
     * @param ids 需要删除的联系我们主键集合
     * @return 结果
     */
    public int deleteOpiContactByIds(Long[] ids);

    /**
     * 删除联系我们信息
     * 
     * @param id 联系我们主键
     * @return 结果
     */
    public int deleteOpiContactById(Long id);

}
