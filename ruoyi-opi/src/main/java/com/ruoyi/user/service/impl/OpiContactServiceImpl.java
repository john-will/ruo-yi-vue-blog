package com.ruoyi.user.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.user.mapper.OpiContactMapper;
import com.ruoyi.user.domain.OpiContact;
import com.ruoyi.user.service.IOpiContactService;

/**
 * 联系我们Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
@Service
public class OpiContactServiceImpl implements IOpiContactService 
{
    @Autowired
    private OpiContactMapper opiContactMapper;

    /**
     * 查询联系我们
     * 
     * @param id 联系我们主键
     * @return 联系我们
     */
    @Override
    public OpiContact selectOpiContactById(Long id)
    {
        return opiContactMapper.selectOpiContactById(id);
    }

    /**
     * 查询联系我们列表
     * 
     * @param opiContact 联系我们
     * @return 联系我们
     */
    @Override
    public List<OpiContact> selectOpiContactList(OpiContact opiContact)
    {
        return opiContactMapper.selectOpiContactList(opiContact);
    }

    /**
     * 新增联系我们
     * 
     * @param opiContact 联系我们
     * @return 结果
     */
    @Override
    public int insertOpiContact(OpiContact opiContact)
    {
        return opiContactMapper.insertOpiContact(opiContact);
    }

    /**
     * 修改联系我们
     * 
     * @param opiContact 联系我们
     * @return 结果
     */
    @Override
    public int updateOpiContact(OpiContact opiContact)
    {
        return opiContactMapper.updateOpiContact(opiContact);
    }

    /**
     * 批量删除联系我们
     * 
     * @param ids 需要删除的联系我们主键
     * @return 结果
     */
    @Override
    public int deleteOpiContactByIds(Long[] ids)
    {
        return opiContactMapper.deleteOpiContactByIds(ids);
    }

    /**
     * 删除联系我们信息
     * 
     * @param id 联系我们主键
     * @return 结果
     */
    @Override
    public int deleteOpiContactById(Long id)
    {
        return opiContactMapper.deleteOpiContactById(id);
    }


}
