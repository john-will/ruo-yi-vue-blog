package com.ruoyi.user.service;

import java.util.List;

import com.ruoyi.user.domain.ContactPojo;
import com.ruoyi.user.domain.OpiContact;
import com.ruoyi.user.domain.OpiUsers;

/**
 * 代理商Service接口
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
public interface IOpiUsersService 
{
    /**
     * 查询代理商
     * 
     * @param id 代理商主键
     * @return 代理商
     */
    public OpiUsers selectOpiUsersById(Long id);

    /**
     * 查询代理商列表
     * 
     * @param opiUsers 代理商
     * @return 代理商集合
     */
    public List<ContactPojo> selectOpiUsersList(ContactPojo contact);

    /**
     * 新增代理商
     * 
     * @param contact 代理商
     * @return 结果
     */
    public int insertOpiUsers(ContactPojo contact);

    /**
     * 修改代理商
     * 
     * @param opiUsers 代理商
     * @return 结果
     */
    public int updateOpiUsers(OpiUsers opiUsers);

    /**
     * 批量删除代理商
     * 
     * @param ids 需要删除的代理商主键集合
     * @return 结果
     */
    public int deleteOpiUsersByIds(Long[] ids);

    /**
     * 删除代理商信息
     * 
     * @param id 代理商主键
     * @return 结果
     */
    public int deleteOpiUsersById(Long id);


}
