package com.ruoyi.user.service.impl;

import java.util.List;

import com.ruoyi.user.domain.ContactPojo;
import com.ruoyi.user.domain.OpiContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.user.mapper.OpiUsersMapper;
import com.ruoyi.user.domain.OpiUsers;
import com.ruoyi.user.service.IOpiUsersService;

/**
 * 代理商Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
@Service
public class OpiUsersServiceImpl implements IOpiUsersService 
{
    @Autowired
    private OpiUsersMapper opiUsersMapper;

    /**
     * 查询代理商
     * 
     * @param id 代理商主键
     * @return 代理商
     */
    @Override
    public OpiUsers selectOpiUsersById(Long id)
    {
        return opiUsersMapper.selectOpiUsersById(id);
    }

    /**
     * 查询代理商列表
     * 
     * @param opiUsers 代理商
     * @return 代理商
     */
    @Override
    public List<ContactPojo> selectOpiUsersList(ContactPojo contact)
    {
        return opiUsersMapper.selectOpiUsersList(contact);
    }

    /**
     * 新增代理商
     * 
     * @param contact 代理商
     * @return 结果
     */
    @Override
    public int insertOpiUsers(ContactPojo contact)
    {
        return opiUsersMapper.insertOpiUsers(contact);
    }

    /**
     * 修改代理商
     * 
     * @param opiUsers 代理商
     * @return 结果
     */
    @Override
    public int updateOpiUsers(OpiUsers opiUsers)
    {
        return opiUsersMapper.updateOpiUsers(opiUsers);
    }

    /**
     * 批量删除代理商
     * 
     * @param ids 需要删除的代理商主键
     * @return 结果
     */
    @Override
    public int deleteOpiUsersByIds(Long[] ids)
    {
        return opiUsersMapper.deleteOpiUsersByIds(ids);
    }

    /**
     * 删除代理商信息
     * 
     * @param id 代理商主键
     * @return 结果
     */
    @Override
    public int deleteOpiUsersById(Long id)
    {
        return opiUsersMapper.deleteOpiUsersById(id);
    }


}
