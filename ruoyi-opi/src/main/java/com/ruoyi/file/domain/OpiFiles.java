package com.ruoyi.file.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 附件上传对象 opi_files
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
public class OpiFiles extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 展示在页面上的图片路径 */
    @Excel(name = "展示在页面上的图片路径")
    private String imgSrc;

    /** PDF文件路径 */
    @Excel(name = "PDF文件路径")
    private String pdfSrc;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setImgSrc(String imgSrc) 
    {
        this.imgSrc = imgSrc;
    }

    public String getImgSrc() 
    {
        return imgSrc;
    }
    public void setPdfSrc(String pdfSrc) 
    {
        this.pdfSrc = pdfSrc;
    }

    public String getPdfSrc() 
    {
        return pdfSrc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("imgSrc", getImgSrc())
            .append("pdfSrc", getPdfSrc())
            .toString();
    }
}
