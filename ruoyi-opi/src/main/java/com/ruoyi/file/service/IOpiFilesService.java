package com.ruoyi.file.service;

import java.util.List;
import com.ruoyi.file.domain.OpiFiles;

/**
 * 附件上传Service接口
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
public interface IOpiFilesService 
{
    /**
     * 查询附件上传
     * 
     * @param id 附件上传主键
     * @return 附件上传
     */
    public OpiFiles selectOpiFilesById(Long id);

    /**
     * 查询附件上传列表
     * 
     * @param opiFiles 附件上传
     * @return 附件上传集合
     */
    public List<OpiFiles> selectOpiFilesList(OpiFiles opiFiles);

    /**
     * 新增附件上传
     * 
     * @param opiFiles 附件上传
     * @return 结果
     */
    public int insertOpiFiles(OpiFiles opiFiles);

    /**
     * 修改附件上传
     * 
     * @param opiFiles 附件上传
     * @return 结果
     */
    public int updateOpiFiles(OpiFiles opiFiles);

    /**
     * 批量删除附件上传
     * 
     * @param ids 需要删除的附件上传主键集合
     * @return 结果
     */
    public int deleteOpiFilesByIds(Long[] ids);

    /**
     * 删除附件上传信息
     * 
     * @param id 附件上传主键
     * @return 结果
     */
    public int deleteOpiFilesById(Long id);
}
