package com.ruoyi.file.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.file.mapper.OpiFilesMapper;
import com.ruoyi.file.domain.OpiFiles;
import com.ruoyi.file.service.IOpiFilesService;

/**
 * 附件上传Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
@Service
public class OpiFilesServiceImpl implements IOpiFilesService 
{
    @Autowired
    private OpiFilesMapper opiFilesMapper;

    /**
     * 查询附件上传
     * 
     * @param id 附件上传主键
     * @return 附件上传
     */
    @Override
    public OpiFiles selectOpiFilesById(Long id)
    {
        return opiFilesMapper.selectOpiFilesById(id);
    }

    /**
     * 查询附件上传列表
     * 
     * @param opiFiles 附件上传
     * @return 附件上传
     */
    @Override
    public List<OpiFiles> selectOpiFilesList(OpiFiles opiFiles)
    {
        return opiFilesMapper.selectOpiFilesList(opiFiles);
    }

    /**
     * 新增附件上传
     * 
     * @param opiFiles 附件上传
     * @return 结果
     */
    @Override
    public int insertOpiFiles(OpiFiles opiFiles)
    {
        return opiFilesMapper.insertOpiFiles(opiFiles);
    }

    /**
     * 修改附件上传
     * 
     * @param opiFiles 附件上传
     * @return 结果
     */
    @Override
    public int updateOpiFiles(OpiFiles opiFiles)
    {
        return opiFilesMapper.updateOpiFiles(opiFiles);
    }

    /**
     * 批量删除附件上传
     * 
     * @param ids 需要删除的附件上传主键
     * @return 结果
     */
    @Override
    public int deleteOpiFilesByIds(Long[] ids)
    {
        return opiFilesMapper.deleteOpiFilesByIds(ids);
    }

    /**
     * 删除附件上传信息
     * 
     * @param id 附件上传主键
     * @return 结果
     */
    @Override
    public int deleteOpiFilesById(Long id)
    {
        return opiFilesMapper.deleteOpiFilesById(id);
    }
}
