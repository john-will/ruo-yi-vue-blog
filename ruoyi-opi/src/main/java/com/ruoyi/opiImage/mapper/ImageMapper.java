package com.ruoyi.opiImage.mapper;


import com.ruoyi.opiImage.domain.OpiImages;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ImageMapper {

    // 查询所有最新的图片
    @Select("SELECT * FROM opi_images WHERE status = #{isShow} ORDER BY create_time DESC")
    List<OpiImages> queryAll(@Param("isShow") Integer isShow);

}
