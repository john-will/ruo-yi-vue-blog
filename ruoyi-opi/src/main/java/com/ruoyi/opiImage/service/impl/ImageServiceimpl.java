package com.ruoyi.opiImage.service.impl;

import com.ruoyi.opiImage.domain.OpiImages;
import com.ruoyi.opiImage.mapper.ImageMapper;
import com.ruoyi.opiImage.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceimpl implements ImageService {

    @Autowired
    private ImageMapper imageMapper;

    @Override
    public List<OpiImages> queryAll(Integer isShow) {
         return imageMapper.queryAll(isShow);
    }



}
