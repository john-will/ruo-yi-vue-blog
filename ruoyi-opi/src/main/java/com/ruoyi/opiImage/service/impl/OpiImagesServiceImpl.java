package com.ruoyi.opiImage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.opiImage.mapper.OpiImagesMapper;
import com.ruoyi.opiImage.domain.OpiImages;
import com.ruoyi.opiImage.service.IOpiImagesService;

/**
 * 轮播图管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-20
 */
@Service
public class OpiImagesServiceImpl implements IOpiImagesService 
{
    @Autowired
    private OpiImagesMapper opiImagesMapper;

    /**
     * 查询轮播图管理
     * 
     * @param id 轮播图管理主键
     * @return 轮播图管理
     */
    @Override
    public OpiImages selectOpiImagesById(Long id)
    {
        return opiImagesMapper.selectOpiImagesById(id);
    }

    /**
     * 查询轮播图管理列表
     * 
     * @param opiImages 轮播图管理
     * @return 轮播图管理
     */
    @Override
    public List<OpiImages> selectOpiImagesList(OpiImages opiImages)
    {
        return opiImagesMapper.selectOpiImagesList(opiImages);
    }

    /**
     * 新增轮播图管理
     * 
     * @param opiImages 轮播图管理
     * @return 结果
     */
    @Override
    public int insertOpiImages(OpiImages opiImages)
    {
        opiImages.setCreateTime(DateUtils.getNowDate());
        return opiImagesMapper.insertOpiImages(opiImages);
    }

    /**
     * 修改轮播图管理
     * 
     * @param opiImages 轮播图管理
     * @return 结果
     */
    @Override
    public int updateOpiImages(OpiImages opiImages)
    {
        return opiImagesMapper.updateOpiImages(opiImages);
    }

    /**
     * 批量删除轮播图管理
     * 
     * @param ids 需要删除的轮播图管理主键
     * @return 结果
     */
    @Override
    public int deleteOpiImagesByIds(Long[] ids)
    {
        return opiImagesMapper.deleteOpiImagesByIds(ids);
    }

    /**
     * 删除轮播图管理信息
     * 
     * @param id 轮播图管理主键
     * @return 结果
     */
    @Override
    public int deleteOpiImagesById(Long id)
    {
        return opiImagesMapper.deleteOpiImagesById(id);
    }
}
