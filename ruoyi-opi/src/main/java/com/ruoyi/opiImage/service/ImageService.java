package com.ruoyi.opiImage.service;

import com.ruoyi.opiImage.domain.OpiImages;

import java.util.List;

public interface ImageService {

    /**
     * 查询所有轮播图列表
     * @param isShow
     * @return
     */
    public List<OpiImages> queryAll(Integer isShow);


}
