package com.ruoyi.opiImage.service;

import java.util.List;
import com.ruoyi.opiImage.domain.OpiImages;

/**
 * 轮播图管理Service接口
 * 
 * @author ruoyi
 * @date 2024-03-20
 */
public interface IOpiImagesService 
{
    /**
     * 查询轮播图管理
     * 
     * @param id 轮播图管理主键
     * @return 轮播图管理
     */
    public OpiImages selectOpiImagesById(Long id);

    /**
     * 查询轮播图管理列表
     * 
     * @param opiImages 轮播图管理
     * @return 轮播图管理集合
     */
    public List<OpiImages> selectOpiImagesList(OpiImages opiImages);

    /**
     * 新增轮播图管理
     * 
     * @param opiImages 轮播图管理
     * @return 结果
     */
    public int insertOpiImages(OpiImages opiImages);

    /**
     * 修改轮播图管理
     * 
     * @param opiImages 轮播图管理
     * @return 结果
     */
    public int updateOpiImages(OpiImages opiImages);

    /**
     * 批量删除轮播图管理
     * 
     * @param ids 需要删除的轮播图管理主键集合
     * @return 结果
     */
    public int deleteOpiImagesByIds(Long[] ids);

    /**
     * 删除轮播图管理信息
     * 
     * @param id 轮播图管理主键
     * @return 结果
     */
    public int deleteOpiImagesById(Long id);
}
