package com.ruoyi.opiImage.domain;
 
public enum YesOrNo {
    /**
     * 表示是否的枚举
     */
    YES(0, "是"),
    NO(1, "否");
 
    public final int type;
    public final String value;
 
    YesOrNo(int type, String value) {
        this.type = type;
        this.value = value;
    }
}