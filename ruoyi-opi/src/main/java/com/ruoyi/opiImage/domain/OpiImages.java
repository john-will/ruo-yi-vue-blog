package com.ruoyi.opiImage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * 轮播图管理对象 opi_images
 * 
 * @author ruoyi
 * @date 2024-03-20
 */
public class OpiImages extends BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String url;

    /** 图片标头 */
    @Excel(name = "图片标头")
    private String title;

    /** 图片顺序 */
    @Excel(name = "图片顺序")
    private Long orderIndex;

    /** 轮播图标识 */
    @Excel(name = "轮播图标识")
    private Integer isCarousel;

    /** 图片状态 */
    @Excel(name = "图片状态")
    private Integer status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setOrderIndex(Long orderIndex) 
    {
        this.orderIndex = orderIndex;
    }

    public Long getOrderIndex() 
    {
        return orderIndex;
    }
    public void setIsCarousel(Integer isCarousel) 
    {
        this.isCarousel = isCarousel;
    }

    public Integer getIsCarousel() 
    {
        return isCarousel;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("url", getUrl())
            .append("title", getTitle())
            .append("orderIndex", getOrderIndex())
            .append("isCarousel", getIsCarousel())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .toString();
    }
}
