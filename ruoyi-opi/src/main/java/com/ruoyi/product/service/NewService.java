package com.ruoyi.product.service;


import com.github.pagehelper.Page;
import com.ruoyi.product.domain.PageResult;
import com.ruoyi.product.domain.dto.NewQueryDTO;
import com.ruoyi.product.domain.vo.NewDetailVO;
import com.ruoyi.product.domain.vo.NewQueryVO;

import java.util.List;

public interface NewService {



    /**
     * 查询新闻
     * @param
     * @return
     */
    List<NewQueryVO> queryList(int offset, int size);


    /**
     * 查询新闻详情
     * @param id
     * @return
     */
    NewDetailVO queryDetail(Long id);


}
