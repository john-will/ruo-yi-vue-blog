package com.ruoyi.product.service;

import java.util.List;
import com.ruoyi.product.domain.OpiDetail;

/**
 * 产品明细Service接口
 * 
 * @author ruoyi
 * @date 2024-03-19
 */
public interface IOpiDetailService 
{
    /**
     * 查询产品明细
     * 
     * @param id 产品明细主键
     * @return 产品明细
     */
    public OpiDetail selectOpiDetailById(Long id);

    /**
     * 查询产品明细列表
     * 
     * @param opiDetail 产品明细
     * @return 产品明细集合
     */
    public List<OpiDetail> selectOpiDetailList(OpiDetail opiDetail);

    /**
     * 新增产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    public int insertOpiDetail(OpiDetail opiDetail);

    /**
     * 修改产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    public int updateOpiDetail(OpiDetail opiDetail);

    /**
     * 批量删除产品明细
     * 
     * @param ids 需要删除的产品明细主键集合
     * @return 结果
     */
    public int deleteOpiDetailByIds(Long[] ids);

    /**
     * 删除产品明细信息
     * 
     * @param id 产品明细主键
     * @return 结果
     */
    public int deleteOpiDetailById(Long id);
}
