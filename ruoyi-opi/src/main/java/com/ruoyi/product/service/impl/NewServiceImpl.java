package com.ruoyi.product.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ruoyi.product.domain.PageResult;
import com.ruoyi.product.domain.dto.NewQueryDTO;
import com.ruoyi.product.domain.vo.NewDetailVO;
import com.ruoyi.product.domain.vo.NewQueryVO;
import com.ruoyi.product.mapper.NewMapper;
import com.ruoyi.product.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewServiceImpl implements NewService {

    @Autowired
    private NewMapper newMapper;

    /**
     * 查询新闻列表
     *
     * @param
     * @return
     */
    @Override
    public List<NewQueryVO> queryList(int offset, int size) {
        // 使用offset和limit构造分页查询SQL语句
        return newMapper.selectAllWithPage(offset, size);
    }

    /**
     * 查询详情,根据id
     * @param id
     * @return
     */
    @Override
    public NewDetailVO queryDetail(Long id) {
        NewDetailVO newDetailVO = newMapper.queryDetail(id);
        return newDetailVO;
    }
}
