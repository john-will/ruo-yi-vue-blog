package com.ruoyi.product.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.OpiListMapper;
import com.ruoyi.product.domain.OpiList;
import com.ruoyi.product.service.IOpiListService;

/**
 * 产品分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
@Service
public class OpiListServiceImpl implements IOpiListService 
{
    @Autowired
    private OpiListMapper opiListMapper;

    /**
     * 查询产品分类
     * 
     * @param id 产品分类主键
     * @return 产品分类
     */
    @Override
    public OpiList selectOpiListById(Long id)
    {
        return opiListMapper.selectOpiListById(id);
    }

    /**
     * 查询产品分类列表
     * 
     * @param opiList 产品分类
     * @return 产品分类
     */
    @Override
    public List<OpiList> selectOpiListList(OpiList opiList)
    {
        return opiListMapper.selectOpiListList(opiList);
    }

    /**
     * 新增产品分类
     * 
     * @param opiList 产品分类
     * @return 结果
     */
    @Override
    public int insertOpiList(OpiList opiList)
    {
        return opiListMapper.insertOpiList(opiList);
    }

    /**
     * 修改产品分类
     * 
     * @param opiList 产品分类
     * @return 结果
     */
    @Override
    public int updateOpiList(OpiList opiList)
    {
        return opiListMapper.updateOpiList(opiList);
    }

    /**
     * 批量删除产品分类
     * 
     * @param ids 需要删除的产品分类主键
     * @return 结果
     */
    @Override
    public int deleteOpiListByIds(Long[] ids)
    {
        return opiListMapper.deleteOpiListByIds(ids);
    }

    /**
     * 删除产品分类信息
     * 
     * @param id 产品分类主键
     * @return 结果
     */
    @Override
    public int deleteOpiListById(Long id)
    {
        return opiListMapper.deleteOpiListById(id);
    }
}
