package com.ruoyi.product.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.product.mapper.OpiDetailMapper;
import com.ruoyi.product.domain.OpiDetail;
import com.ruoyi.product.service.IOpiDetailService;

/**
 * 产品明细Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-19
 */
@Service
public class OpiDetailServiceImpl implements IOpiDetailService 
{
    @Autowired
    private OpiDetailMapper opiDetailMapper;

    /**
     * 查询产品明细
     * 
     * @param id 产品明细主键
     * @return 产品明细
     */
    @Override
    public OpiDetail selectOpiDetailById(Long id)
    {
        return opiDetailMapper.selectOpiDetailById(id);
    }

    /**
     * 查询产品明细列表
     * 
     * @param opiDetail 产品明细
     * @return 产品明细
     */
    @Override
    public List<OpiDetail> selectOpiDetailList(OpiDetail opiDetail)
    {
        return opiDetailMapper.selectOpiDetailList(opiDetail);
    }

    /**
     * 新增产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    @Override
    public int insertOpiDetail(OpiDetail opiDetail)
    {
        return opiDetailMapper.insertOpiDetail(opiDetail);
    }

    /**
     * 修改产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    @Override
    public int updateOpiDetail(OpiDetail opiDetail)
    {
        return opiDetailMapper.updateOpiDetail(opiDetail);
    }

    /**
     * 批量删除产品明细
     * 
     * @param ids 需要删除的产品明细主键
     * @return 结果
     */
    @Override
    public int deleteOpiDetailByIds(Long[] ids)
    {
        return opiDetailMapper.deleteOpiDetailByIds(ids);
    }

    /**
     * 删除产品明细信息
     * 
     * @param id 产品明细主键
     * @return 结果
     */
    @Override
    public int deleteOpiDetailById(Long id)
    {
        return opiDetailMapper.deleteOpiDetailById(id);
    }
}
