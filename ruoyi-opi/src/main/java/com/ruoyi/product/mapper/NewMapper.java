package com.ruoyi.product.mapper;

import com.github.pagehelper.Page;
import com.ruoyi.product.domain.dto.NewQueryDTO;
import com.ruoyi.product.domain.vo.NewDetailVO;
import com.ruoyi.product.domain.vo.NewQueryVO;
import org.apache.ibatis.annotations.*;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

public interface NewMapper {

    /**
     * 查询所有
     * @return
     */
    @Select("SELECT id, blog_pic AS blogPic, blog_desc AS blogDesc, create_time AS createTime, title FROM cms_blog ORDER BY create_time DESC LIMIT #{offset}, #{size}")
    List<NewQueryVO> selectAllWithPage(@Param("offset") int offset, @Param("size") int size);



    /**
     * 查询详情
     * @param id
     * @return
     */
    NewDetailVO queryDetail(Long id);

}

