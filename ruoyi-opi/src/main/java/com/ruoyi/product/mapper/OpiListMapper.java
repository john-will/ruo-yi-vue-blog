package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.OpiList;

/**
 * 产品分类Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
public interface OpiListMapper 
{
    /**
     * 查询产品分类
     * 
     * @param id 产品分类主键
     * @return 产品分类
     */
    public OpiList selectOpiListById(Long id);

    /**
     * 查询产品分类列表
     * 
     * @param opiList 产品分类
     * @return 产品分类集合
     */
    public List<OpiList> selectOpiListList(OpiList opiList);

    /**
     * 新增产品分类
     * 
     * @param opiList 产品分类
     * @return 结果
     */
    public int insertOpiList(OpiList opiList);

    /**
     * 修改产品分类
     * 
     * @param opiList 产品分类
     * @return 结果
     */
    public int updateOpiList(OpiList opiList);

    /**
     * 删除产品分类
     * 
     * @param id 产品分类主键
     * @return 结果
     */
    public int deleteOpiListById(Long id);

    /**
     * 批量删除产品分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOpiListByIds(Long[] ids);
}
