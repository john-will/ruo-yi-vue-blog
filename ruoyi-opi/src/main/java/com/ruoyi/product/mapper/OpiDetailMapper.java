package com.ruoyi.product.mapper;

import java.util.List;
import com.ruoyi.product.domain.OpiDetail;

/**
 * 产品明细Mapper接口
 * 
 * @author ruoyi
 * @date 2024-03-19
 */
public interface OpiDetailMapper 
{
    /**
     * 查询产品明细
     * 
     * @param id 产品明细主键
     * @return 产品明细
     */
    public OpiDetail selectOpiDetailById(Long id);

    /**
     * 查询产品明细列表
     * 
     * @param opiDetail 产品明细
     * @return 产品明细集合
     */
    public List<OpiDetail> selectOpiDetailList(OpiDetail opiDetail);

    /**
     * 新增产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    public int insertOpiDetail(OpiDetail opiDetail);

    /**
     * 修改产品明细
     * 
     * @param opiDetail 产品明细
     * @return 结果
     */
    public int updateOpiDetail(OpiDetail opiDetail);

    /**
     * 删除产品明细
     * 
     * @param id 产品明细主键
     * @return 结果
     */
    public int deleteOpiDetailById(Long id);

    /**
     * 批量删除产品明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOpiDetailByIds(Long[] ids);
}
