package com.ruoyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品分类对象 opi_list
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
public class OpiList extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String typename;

    /** 图片 */
    @Excel(name = "图片")
    private String imgsrc;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTypename(String typename) 
    {
        this.typename = typename;
    }

    public String getTypename() 
    {
        return typename;
    }
    public void setImgsrc(String imgsrc) 
    {
        this.imgsrc = imgsrc;
    }

    public String getImgsrc() 
    {
        return imgsrc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("typename", getTypename())
            .append("imgsrc", getImgsrc())
            .toString();
    }
}
