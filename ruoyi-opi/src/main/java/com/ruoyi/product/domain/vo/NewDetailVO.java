package com.ruoyi.product.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewDetailVO {

    // 首页图片 封面
    private String blogPicLink;

    // 简介
    private String blogDesc;

    //创建时间
    private String createTime;

    // 标头
    private String title;

    // 内容
    private String content;
}
