package com.ruoyi.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品明细对象 opi_detail
 * 
 * @author ruoyi
 * @date 2024-03-19
 */
public class OpiDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 产品分类ID */
    @Excel(name = "产品分类ID")
    private Long listId;

    /** 型号 */
    @Excel(name = "型号")
    private String no;

    /** 材料 */
    @Excel(name = "材料")
    private String material;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 外形特征 */
    @Excel(name = "外形特征")
    private String features;

    /** 鄂式类型 */
    @Excel(name = "鄂式类型")
    private String jawType;

    /** 鄂式长度 */
    @Excel(name = "鄂式长度")
    private String jawLength;

    /** 腿部描述 */
    @Excel(name = "腿部描述")
    private String shanks;

    /** 整体长度 */
    @Excel(name = "整体长度")
    private String length;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setListId(Long listId) 
    {
        this.listId = listId;
    }

    public Long getListId() 
    {
        return listId;
    }
    public void setNo(String no) 
    {
        this.no = no;
    }

    public String getNo() 
    {
        return no;
    }
    public void setMaterial(String material) 
    {
        this.material = material;
    }

    public String getMaterial() 
    {
        return material;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setFeatures(String features) 
    {
        this.features = features;
    }

    public String getFeatures() 
    {
        return features;
    }
    public void setJawType(String jawType) 
    {
        this.jawType = jawType;
    }

    public String getJawType() 
    {
        return jawType;
    }
    public void setJawLength(String jawLength) 
    {
        this.jawLength = jawLength;
    }

    public String getJawLength() 
    {
        return jawLength;
    }
    public void setShanks(String shanks) 
    {
        this.shanks = shanks;
    }

    public String getShanks() 
    {
        return shanks;
    }
    public void setLength(String length) 
    {
        this.length = length;
    }

    public String getLength() 
    {
        return length;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("listId", getListId())
            .append("no", getNo())
            .append("material", getMaterial())
            .append("type", getType())
            .append("features", getFeatures())
            .append("jawType", getJawType())
            .append("jawLength", getJawLength())
            .append("shanks", getShanks())
            .append("length", getLength())
            .toString();
    }
}
