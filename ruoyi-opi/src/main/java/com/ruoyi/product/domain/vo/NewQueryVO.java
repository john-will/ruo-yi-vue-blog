package com.ruoyi.product.domain.vo;

import com.ruoyi.common.annotation.DataSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewQueryVO {


    // id
    private Integer id;

    // 首页图片 封面
    private String blogPic;

    // 简介
    private String blogDesc;

    //创建时间
    private String createTime;

    // 标头
    private String title;


}
