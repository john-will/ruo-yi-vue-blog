import request from '@/utils/request'

// 查询产品明细列表
export function listDetail(query) {
  return request({
    url: '/product/detail/list',
    method: 'get',
    params: query
  })
}

// 查询产品明细详细
export function getDetail(id) {
  return request({
    url: '/product/detail/' + id,
    method: 'get'
  })
}

// 新增产品明细
export function addDetail(data) {
  return request({
    url: '/product/detail',
    method: 'post',
    data: data
  })
}

// 修改产品明细
export function updateDetail(data) {
  return request({
    url: '/product/detail',
    method: 'put',
    data: data
  })
}

// 删除产品明细
export function delDetail(id) {
  return request({
    url: '/product/detail/' + id,
    method: 'delete'
  })
}
