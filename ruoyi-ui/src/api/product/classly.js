import request from '@/utils/request'

// 查询产品分类列表
export function listClassly(query) {
  return request({
    url: '/product/classly/list',
    method: 'get',
    params: query
  })
}

// 查询产品分类详细
export function getClassly(id) {
  return request({
    url: '/product/classly/' + id,
    method: 'get'
  })
}

// 新增产品分类
export function addClassly(data) {
  return request({
    url: '/product/classly',
    method: 'post',
    data: data
  })
}

// 修改产品分类
export function updateClassly(data) {
  return request({
    url: '/product/classly',
    method: 'put',
    data: data
  })
}

// 删除产品分类
export function delClassly(id) {
  return request({
    url: '/product/classly/' + id,
    method: 'delete'
  })
}
