import request from '@/utils/request'

// 查询附件上传列表
export function listFiles(query) {
  return request({
    url: '/file/files/list',
    method: 'get',
    params: query
  })
}

// 查询附件上传详细
export function getFiles(id) {
  return request({
    url: '/file/files/' + id,
    method: 'get'
  })
}

// 新增附件上传
export function addFiles(data) {
  return request({
    url: '/file/files',
    method: 'post',
    data: data
  })
}

// 修改附件上传
export function updateFiles(data) {
  return request({
    url: '/file/files',
    method: 'put',
    data: data
  })
}

// 删除附件上传
export function delFiles(id) {
  return request({
    url: '/file/files/' + id,
    method: 'delete'
  })
}
