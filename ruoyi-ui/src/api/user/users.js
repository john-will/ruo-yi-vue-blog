import request from '@/utils/request'

// 查询联系我们列表
export function listUsers(query) {
  return request({
    url: '/user/users/list',
    method: 'get',
    params: query
  })
}

// 查询联系我们详细
export function getUsers(id) {
  return request({
    url: '/user/users/' + id,
    method: 'get'
  })
}

// 新增联系我们
export function addUsers(data) {
  return request({
    url: '/user/users',
    method: 'post',
    data: data
  })
}

// 修改联系我们
export function updateUsers(data) {
  return request({
    url: '/user/users',
    method: 'put',
    data: data
  })
}

// 删除联系我们
export function delUsers(id) {
  return request({
    url: '/user/users/' + id,
    method: 'delete'
  })
}
