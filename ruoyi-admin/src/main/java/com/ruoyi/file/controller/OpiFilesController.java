package com.ruoyi.file.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.file.domain.OpiFiles;
import com.ruoyi.file.service.IOpiFilesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 附件上传Controller
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
@RestController
@RequestMapping("/file/files")
public class OpiFilesController extends BaseController
{
    @Autowired
    private IOpiFilesService opiFilesService;

    /**
     * 查询附件上传列表
     */
    @PreAuthorize("@ss.hasPermi('file:files:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpiFiles opiFiles)
    {
        startPage();
        List<OpiFiles> list = opiFilesService.selectOpiFilesList(opiFiles);
        return getDataTable(list);
    }

    /**
     * 导出附件上传列表
     */
    @PreAuthorize("@ss.hasPermi('file:files:export')")
    @Log(title = "附件上传", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpiFiles opiFiles)
    {
        List<OpiFiles> list = opiFilesService.selectOpiFilesList(opiFiles);
        ExcelUtil<OpiFiles> util = new ExcelUtil<OpiFiles>(OpiFiles.class);
        util.exportExcel(response, list, "附件上传数据");
    }

    /**
     * 获取附件上传详细信息
     */
    @PreAuthorize("@ss.hasPermi('file:files:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(opiFilesService.selectOpiFilesById(id));
    }

    /**
     * 新增附件上传
     */
    @PreAuthorize("@ss.hasPermi('file:files:add')")
    @Log(title = "附件上传", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpiFiles opiFiles)
    {
        return toAjax(opiFilesService.insertOpiFiles(opiFiles));
    }

    /**
     * 修改附件上传
     */
    @PreAuthorize("@ss.hasPermi('file:files:edit')")
    @Log(title = "附件上传", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpiFiles opiFiles)
    {
        return toAjax(opiFilesService.updateOpiFiles(opiFiles));
    }

    /**
     * 删除附件上传
     */
    @PreAuthorize("@ss.hasPermi('file:files:remove')")
    @Log(title = "附件上传", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(opiFilesService.deleteOpiFilesByIds(ids));
    }
}
