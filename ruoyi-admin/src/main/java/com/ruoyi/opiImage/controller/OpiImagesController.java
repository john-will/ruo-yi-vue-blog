package com.ruoyi.opiImage.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.opiImage.domain.OpiImages;
import com.ruoyi.opiImage.service.IOpiImagesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 轮播图管理Controller
 * 
 * @author ruoyi
 * @date 2024-03-20
 */
@RestController
@RequestMapping("/image/images")
public class OpiImagesController extends BaseController
{
    @Autowired
    private IOpiImagesService opiImagesService;

    /**
     * 查询轮播图管理列表
     */
    @PreAuthorize("@ss.hasPermi('image:images:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpiImages opiImages)
    {
        startPage();
        List<OpiImages> list = opiImagesService.selectOpiImagesList(opiImages);
        return getDataTable(list);
    }

    /**
     * 导出轮播图管理列表
     */
    @PreAuthorize("@ss.hasPermi('image:images:export')")
    @Log(title = "轮播图管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpiImages opiImages)
    {
        List<OpiImages> list = opiImagesService.selectOpiImagesList(opiImages);
        ExcelUtil<OpiImages> util = new ExcelUtil<OpiImages>(OpiImages.class);
        util.exportExcel(response, list, "轮播图管理数据");
    }

    /**
     * 获取轮播图管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('image:images:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(opiImagesService.selectOpiImagesById(id));
    }

    /**
     * 新增轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('image:images:add')")
    @Log(title = "轮播图管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpiImages opiImages)
    {
        return toAjax(opiImagesService.insertOpiImages(opiImages));
    }

    /**
     * 修改轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('image:images:edit')")
    @Log(title = "轮播图管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpiImages opiImages)
    {
        return toAjax(opiImagesService.updateOpiImages(opiImages));
    }

    /**
     * 删除轮播图管理
     */
    @PreAuthorize("@ss.hasPermi('image:images:remove')")
    @Log(title = "轮播图管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(opiImagesService.deleteOpiImagesByIds(ids));
    }
}
