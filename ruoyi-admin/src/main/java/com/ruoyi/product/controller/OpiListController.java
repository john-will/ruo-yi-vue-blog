package com.ruoyi.product.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.product.domain.OpiList;
import com.ruoyi.product.service.IOpiListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 产品分类Controller
 * 
 * @author ruoyi
 * @date 2024-03-18
 */
@RestController
@RequestMapping("/product/classly")
public class OpiListController extends BaseController
{
    @Autowired
    private IOpiListService opiListService;

    /**
     * 查询产品分类列表
     */
    @PreAuthorize("@ss.hasPermi('product:classly:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpiList opiList)
    {
        startPage();
        List<OpiList> list = opiListService.selectOpiListList(opiList);
        return getDataTable(list);
    }

    /**
     * 导出产品分类列表
     */
    @PreAuthorize("@ss.hasPermi('product:classly:export')")
    @Log(title = "产品分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpiList opiList)
    {
        List<OpiList> list = opiListService.selectOpiListList(opiList);
        ExcelUtil<OpiList> util = new ExcelUtil<OpiList>(OpiList.class);
        util.exportExcel(response, list, "产品分类数据");
    }

    /**
     * 获取产品分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:classly:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(opiListService.selectOpiListById(id));
    }

    /**
     * 新增产品分类
     */
    @PreAuthorize("@ss.hasPermi('product:classly:add')")
    @Log(title = "产品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpiList opiList)
    {
        return toAjax(opiListService.insertOpiList(opiList));
    }

    /**
     * 修改产品分类
     */
    @PreAuthorize("@ss.hasPermi('product:classly:edit')")
    @Log(title = "产品分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpiList opiList)
    {
        return toAjax(opiListService.updateOpiList(opiList));
    }

    /**
     * 删除产品分类
     */
    @PreAuthorize("@ss.hasPermi('product:classly:remove')")
    @Log(title = "产品分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(opiListService.deleteOpiListByIds(ids));
    }
}
