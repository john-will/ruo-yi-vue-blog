package com.ruoyi.product.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.product.domain.OpiDetail;
import com.ruoyi.product.service.IOpiDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品明细Controller
 * 
 * @author ruoyi
 * @date 2024-03-19
 */
@RestController
@RequestMapping("/product/detail")
public class OpiDetailController extends BaseController
{
    @Autowired
    private IOpiDetailService opiDetailService;

    /**
     * 查询产品明细列表
     */
    @PreAuthorize("@ss.hasPermi('product:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpiDetail opiDetail)
    {
        startPage();
        List<OpiDetail> list = opiDetailService.selectOpiDetailList(opiDetail);
        return getDataTable(list);
    }

    /**
     * 导出产品明细列表
     */
    @PreAuthorize("@ss.hasPermi('product:detail:export')")
    @Log(title = "产品明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpiDetail opiDetail)
    {
        List<OpiDetail> list = opiDetailService.selectOpiDetailList(opiDetail);
        ExcelUtil<OpiDetail> util = new ExcelUtil<OpiDetail>(OpiDetail.class);
        util.exportExcel(response, list, "产品明细数据");
    }

    /**
     * 获取产品明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('product:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(opiDetailService.selectOpiDetailById(id));
    }

    /**
     * 新增产品明细
     */
    @PreAuthorize("@ss.hasPermi('product:detail:add')")
    @Log(title = "产品明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpiDetail opiDetail)
    {
        return toAjax(opiDetailService.insertOpiDetail(opiDetail));
    }

    /**
     * 修改产品明细
     */
    @PreAuthorize("@ss.hasPermi('product:detail:edit')")
    @Log(title = "产品明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpiDetail opiDetail)
    {
        return toAjax(opiDetailService.updateOpiDetail(opiDetail));
    }

    /**
     * 删除产品明细
     */
    @PreAuthorize("@ss.hasPermi('product:detail:remove')")
    @Log(title = "产品明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(opiDetailService.deleteOpiDetailByIds(ids));
    }
}
