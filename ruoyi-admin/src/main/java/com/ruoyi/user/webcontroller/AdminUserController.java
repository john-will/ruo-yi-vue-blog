package com.ruoyi.user.webcontroller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.user.domain.ContactPojo;
import com.ruoyi.user.domain.OpiContact;
import com.ruoyi.user.domain.OpiUsers;
import com.ruoyi.user.service.IOpiContactService;
import com.ruoyi.user.service.IOpiUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/web/users")
public class AdminUserController extends BaseController {

    @Autowired
    private IOpiUsersService opiUsersService;

    @Autowired
    private IOpiContactService opiContactService;

    /**
     * 新增代理商
     */
    @Log(title = "代理商", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ContactPojo contact)
    {
        return toAjax(opiUsersService.insertOpiUsers(contact));
    }


    /**
     * 回显地址
     * @return
     */
    @GetMapping("/addr")
    public AjaxResult selectAddr() {
        List<OpiContact> opiContacts = opiContactService.selectOpiContactList(new OpiContact());
        return AjaxResult.success(opiContacts);
    }

}
