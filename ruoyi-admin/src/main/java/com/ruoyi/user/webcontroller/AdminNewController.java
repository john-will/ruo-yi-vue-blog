package com.ruoyi.user.webcontroller;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.product.domain.PageResult;
import com.ruoyi.product.domain.dto.NewQueryDTO;
import com.ruoyi.product.domain.vo.NewDetailVO;
import com.ruoyi.product.domain.vo.NewQueryVO;
import com.ruoyi.product.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/web/news")
public class AdminNewController {


    @Autowired
    private NewService newService;


    /**
     * 查询新闻列表
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/new")
    public AjaxResult<List<NewQueryVO>> newList(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "4") int size) {
        if (page < 1) {
            return null;
        }
        int offset = (page - 1) * size;
        List<NewQueryVO> newQueryVOS = newService.queryList(offset, size);
        return AjaxResult.success(newQueryVOS);
    }



    /**
     * 前台查询新闻详情
     * @param id
     * @return
     */
    @GetMapping("/list/{id}")
    public AjaxResult<NewDetailVO>  newDetail(@PathVariable("id")Long id) {
        System.out.println("新闻详情查询 : "+id.toString());
        NewDetailVO newDetailVO =newService.queryDetail(id);
        return AjaxResult.success(newDetailVO);
    }

}
