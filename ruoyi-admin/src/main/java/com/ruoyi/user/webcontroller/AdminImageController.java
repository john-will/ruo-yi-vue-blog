package com.ruoyi.user.webcontroller;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.opiImage.domain.OpiImages;
import com.ruoyi.opiImage.domain.YesOrNo;
import com.ruoyi.opiImage.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("/web/image")
public class AdminImageController {


    @Autowired
    private ImageService imageService;


    /**
     * 控制轮播图的图片
     * @return
     */
    @GetMapping("/carousel")
    public AjaxResult carousel() {
        List<OpiImages> list = imageService.queryAll(YesOrNo.YES.type);
        Stream<String> urlStream = list.stream().map(OpiImages::getUrl);
        return AjaxResult.success(urlStream);
    }
}