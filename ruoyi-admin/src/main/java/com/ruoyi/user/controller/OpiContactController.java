package com.ruoyi.user.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.user.domain.OpiContact;
import com.ruoyi.user.service.IOpiContactService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 联系我们Controller
 * 
 * @author ruoyi
 * @date 2024-03-25
 */
@RestController
@RequestMapping("/user/users")
public class OpiContactController extends BaseController
{
    @Autowired
    private IOpiContactService opiContactService;

    /**
     * 查询联系我们列表
     */
    @PreAuthorize("@ss.hasPermi('user:users:list')")
    @GetMapping("/list")
    public TableDataInfo list(OpiContact opiContact)
    {
        startPage();
        List<OpiContact> list = opiContactService.selectOpiContactList(opiContact);
        return getDataTable(list);
    }

    /**
     * 导出联系我们列表
     */
    @PreAuthorize("@ss.hasPermi('user:users:export')")
    @Log(title = "联系我们", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OpiContact opiContact)
    {
        List<OpiContact> list = opiContactService.selectOpiContactList(opiContact);
        ExcelUtil<OpiContact> util = new ExcelUtil<OpiContact>(OpiContact.class);
        util.exportExcel(response, list, "联系我们数据");
    }

    /**
     * 获取联系我们详细信息
     */
    @PreAuthorize("@ss.hasPermi('user:users:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(opiContactService.selectOpiContactById(id));
    }

    /**
     * 新增联系我们
     */
    @PreAuthorize("@ss.hasPermi('user:users:add')")
    @Log(title = "联系我们", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OpiContact opiContact)
    {
        return toAjax(opiContactService.insertOpiContact(opiContact));
    }

    /**
     * 修改联系我们
     */
    @PreAuthorize("@ss.hasPermi('user:users:edit')")
    @Log(title = "联系我们", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OpiContact opiContact)
    {
        return toAjax(opiContactService.updateOpiContact(opiContact));
    }

    /**
     * 删除联系我们
     */
    @PreAuthorize("@ss.hasPermi('user:users:remove')")
    @Log(title = "联系我们", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(opiContactService.deleteOpiContactByIds(ids));
    }
}
